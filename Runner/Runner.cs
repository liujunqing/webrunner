﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace WebRunner
{
    public class Runner
    {
        CookieContainer cookies = new CookieContainer();

        public TestResult Run(TestCase testCase)
        {
            CheckTestCase(testCase);
                  
            //Initial Request
            var req = HttpWebRequest.Create(testCase.Url) as HttpWebRequest;
            req.Method = String.IsNullOrEmpty(testCase.Method) ? "Get" : testCase.Method;
            req.AllowAutoRedirect = true;
            req.CookieContainer = cookies;
            //if post or put
            if (req.Method.Equals("post", StringComparison.OrdinalIgnoreCase) || req.Method.Equals("put", StringComparison.OrdinalIgnoreCase))
            {
                var postData = Encoding.UTF8.GetBytes(testCase.Data);
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = postData.Length;
                using (var requestStream = req.GetRequestStream())
                {
                    requestStream.Write(postData, 0, postData.Length);
                    requestStream.Close();
                }
            }

            TestResult testResult = new TestResult();
            testResult.TestDate = DateTime.Now;
            var begin = DateTime.Now;
            HttpWebResponse resp = null;
            try
            {
                resp = req.GetResponse() as HttpWebResponse;
            }
            catch
            {
                testResult.Pass = false;
                return testResult;
            }
            testResult.ResponseCost = (DateTime.Now - begin).TotalMilliseconds;
            if (resp.StatusCode != HttpStatusCode.OK)
            {
                testResult.Pass = false;
            }
            else
            {
                if (!String.IsNullOrEmpty(resp.Headers["Set-Cookie"]))
                {
                    cookies.SetCookies(resp.ResponseUri, resp.Headers["Set-Cookie"]);
                }
                using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                {
                    var responseBody = sr.ReadToEnd();
                    sr.Close();
                    testResult.Pass = VerifyResponse(testCase, responseBody);
                }
            }
            resp.Close();
            return testResult;
        }

        private Boolean CheckTestCase(TestCase testCase)
        {
            if (String.IsNullOrEmpty(testCase.Url))
            {
                throw new Exception("Url can not be null");
            }
            if (String.IsNullOrEmpty(testCase.Title))
            {
                throw new Exception("Title can not be null");
            }
            return true;
        }

        private Boolean VerifyResponse(TestCase testCase,String responseBody)
        {
            if (String.IsNullOrEmpty(testCase.VerifyPositive))
            {
                return true;
            }
            if (responseBody.Contains(testCase.VerifyPositive))
            {
                return true;
            }
            return false;
        }
    }
}
