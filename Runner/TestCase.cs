﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebRunner
{
    public class TestCase
    {
        public String Title { get; set; }

        public String Description { get; set; }

        public String Url { get; set; }

        public String Method { get; set; }

        public String Data { get; set; }

        public String VerifyPositive { get; set; }

        public Int32 Repeat { get; set; }

        private List<TestResult> testResults = new List<TestResult>();
        public List<TestResult> TestResults
        {
            get
            {
                return testResults;
            }
            set
            {
                testResults = value;
            }
        }
    }

    public class TestResult
    {
        public Boolean Pass { get; set; }

        public Double ResponseCost { get; set; }

        public DateTime TestDate { get; set; }
    }
}
