﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebRunner.Recorder
{
    public interface IRecordProvider
    {
        void Log(TestCase testcase);
    }
}
