﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace WebRunner.Recorder
{
    public class Recorder
    {
        IRecordProvider recorder = null;
        public Recorder(IRecordProvider recordProvider)
        {
            recorder = recordProvider;
        }

        public void Log(TestCase testcase)
        {
            recorder.Log(testcase);
        }

        //String resultPath = String.Format(@"TestResults\{0}", DateTime.Now.ToString("yyyyMMddHHmm"));

        //public void Recorde(TestCase testCase)
        //{
        //    DataTable table = new DataTable("TestCase");
        //    table.Columns.Add("Title");
        //    table.Columns.Add("Url");
        //    table.Columns.Add("Pass");
        //    table.Columns.Add("ResponseCost");

        //    foreach (var result in testCase.TestResults)
        //    {
        //        var row = table.NewRow();
        //        row["Title"] = testCase.Title;
        //        row["Url"] = testCase.Url;
        //        row["Pass"] = result.Pass;
        //        row["ResponseCost"] = result.ResponseCost;
        //        table.Rows.Add(row);
        //    }
            
        //    if (!Directory.Exists(resultPath))
        //    {
        //        Directory.CreateDirectory(resultPath);
        //    }
        //    table.WriteXml(String.Format(@"{0}\{1}.xml",resultPath,testCase.Title));
        //}
    }
}
