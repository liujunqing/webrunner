﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace WebRunner.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var testCases = InitTestCase();

            Runner runner = new Runner();
            var recorder = new WebRunner.Recorder.Recorder(new RecordProvider_DataBase());
            foreach (var testCase in testCases)
            {
                System.Console.WriteLine(testCase.Url);
                if (testCase.Repeat == 0)
                {
                    var testResult = runner.Run(testCase);
                    testCase.TestResults.Add(testResult);
                    System.Console.WriteLine(String.Format("TestCase:{0},Result:{1},GetResponseCost:{2}", testCase.Title, testResult.Pass, testResult.ResponseCost));
                }
                else
                {
                    for (var i = 0; i < testCase.Repeat; i++)
                    {
                        var testResult = runner.Run(testCase);
                        testCase.TestResults.Add(testResult);
                        System.Console.WriteLine(String.Format("TestCase:{0},Result:{1},GetResponseCost:{2}", testCase.Title, testResult.Pass, testResult.ResponseCost));
                    }
                }
                
                recorder.Log(testCase);
            }
        }

        private static List<TestCase> InitTestCase()
        {
            List<TestCase> testCases = new List<TestCase>();
            DataSet ds = new DataSet();
            ds.ReadXml("testcases.xml");
            if (!ds.Tables[0].Columns.Contains("Repeat"))
            {
                ds.Tables[0].Columns.Add("Repeat");
            }
            if (!ds.Tables[0].Columns.Contains("Data"))
            {
                ds.Tables[0].Columns.Add("Data");
            }
            if (!ds.Tables[0].Columns.Contains("Method"))
            {
                ds.Tables[0].Columns.Add("Method");
            }
            if (!ds.Tables[0].Columns.Contains("VerifyPositive"))
            {
                ds.Tables[0].Columns.Add("VerifyPositive");
            }
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var testcase = new TestCase
                {
                    Title = row["Title"].ToString(),
                    Url = System.Web.HttpUtility.UrlDecode(row["Url"].ToString())
                };
                
                testcase.Repeat = String.IsNullOrEmpty(row["Repeat"].ToString()) ? 0 : Int32.Parse(row["Repeat"].ToString());
                testcase.Data = row["Data"].ToString();
                testcase.Method = row["Method"].ToString();
                testcase.VerifyPositive = row["VerifyPositive"].ToString();
                testCases.Add(testcase);
            }

            if (Directory.Exists("testcases"))
            {
                var files = Directory.GetFiles("testcases","*.xml");
                foreach (var file in files)
                {
                    System.Console.WriteLine(file);
                    ds.ReadXml(file);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        testCases.Add(new TestCase
                        {
                            Title = row["Title"].ToString(),
                            Url = HttpUtility.UrlDecode(row["Url"].ToString()),
                            Repeat = String.IsNullOrEmpty(row["Repeat"].ToString()) ? 0 : Int32.Parse(row["Repeat"].ToString()),
                            Data = row["Data"].ToString(),
                            Method = row["Method"].ToString(),
                            VerifyPositive = row["VerifyPositive"].ToString()
                        });
                    }
                }
            }

            return testCases;
        }
    }
}
