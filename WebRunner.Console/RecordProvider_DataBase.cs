﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace WebRunner.Console
{
    public class RecordProvider_DataBase : WebRunner.Recorder.IRecordProvider
    {
        public void Log(TestCase testcase)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DataBase"].ConnectionString))
            {
                con.Open();                
                foreach (var result in testcase.TestResults)
                {
                    var cmd = con.CreateCommand();
                    cmd.CommandText = "insert into [webrunnerrecord] ([Title],[Url],[Pass],[ResponseCost],[TestDate]) values (@title,@url,@pass,@cost,@testdate)";
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "title",
                        Value = testcase.Title
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "url",
                        Value = testcase.Url
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "pass",
                        Value = result.Pass ? 1 : 0
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "cost",
                        Value = result.ResponseCost
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "testdate",
                        Value = result.TestDate
                    });
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
        }
    }
}
